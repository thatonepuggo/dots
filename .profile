addpath() {
    export PATH="$1:$PATH"
}

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export MANROFFOPT="-c"
BAT_CMD="bat"
if [[ "$(grep '^ID=' /etc/os-release | sed 's/^ID=//')" == "debian" ]]; then
    BAT_CMD="batcat"
fi
export MANPAGER="sh -c 'col -bx | $BAT_CMD -plman'"
export VIMRUNTIME="/usr/share/nvim/runtime"
export QT_QPA_PLATFORMTHEME="qt5ct"

export DOOMWADDIR="/mnt/SteamGames/doom/iwads"
export DOOMWADPATH="$DOOMWADDIR:/mnt/SteamGames/doom/mbf"

export EDITOR="nvim"
export VISUAL="nvim"
export TERMINAL="/usr/bin/wezterm"

addpath "$HOME/.local/bin/"
addpath "$HOME/.cargo/bin/"
addpath "$HOME/.config/emacs/bin/"
