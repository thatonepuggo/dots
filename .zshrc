#
#          _
#  _______| |__
# |_  / __| '_ \
#  / /\__ \ | | | config
# /___|___/_| |_|
# by thatonepuggo
#
antigen_dir=$XDG_CACHE_HOME/antigen/
. ~/.profile
. /opt/asdf-vm/asdf.sh

# Relaunch Waybar
rebar() {
    killall waybar
    waybar > /dev/null 2>&1 & disown
}

# runs dosbox with a config in the ~/.config/dosbox directory
# usage: dosboxc [config name]
dosboxc() {
    cd $HOME/.config/dosbox
    if [ "$1" ]; then
        dosbox -conf $1.conf
    else
        dosbox
    fi
    cd - > /dev/null
}

# get location of steam app dir (without cding into it)
# usage: steamwhich [alias|dir name]

STEAMAPPS="$HOME/.steam/steam/steamapps"
declare -A steam_aliases
steam_aliases=(
    [tf2c]="tf2classic"
    [of]="open_fortress"

    [hl]="Half-Life"
    [hl2]="Half-Life 2"

    [tf2]="Team Fortress 2"
    [gd]="Geometry Dash"
    [vh]="Valheim"
    [csgo]="Counter-Strike Global Offensive"
    [p2]="Portal 2"
    [p1]="Portal"

    [doom]="Doom"
    [doom2]="Doom 2"
    )


steamwhich() {
    searchdirs=('common' 'sourcemods')

    getlocation() {
        for dirfind in $searchdirs; do
            ret="$STEAMAPPS/$dirfind/$1"
            if [ -d "$ret" ]; then
                echo "$ret"
            fi
        done
    }

    dir="$STEAMAPPS"
    alias_pick="$steam_aliases[$1]"
    if [ "$1" ]; then
        location=$(getlocation $1)
        [ "$alias_pick" ] && location=$(getlocation $alias_pick)
        dir="$location"
    fi
    echo "$dir"
}

# CD into steam dir
# usage: steamapps [alias|dir name]

steamapps() {
    cd "$(steamwhich $1)"
}



# pingtest - quickly ping to test internet
# usage: pingtest [-c COUNT] [-a ADDRESS]

pingtest() {
    count="4"
    addr="8.8.8.8"
    while getopts 'ca:' OPTION; do
        case "$OPTION" in
            c)
                num="$OPTARG"
            ;;
            a)
                addr="$OPTARG"
            ;;
        esac
    done
    echo "pinging $addr $count times."
    ping -c $count $addr
}

# up - cd into a parent directory
# usage:
# up [AMOUNT]
up() {
    dir=../
    [ "$#" != 0 ] && repeat $(($1 - 1)) dir=${dir}../
    cd $dir
}

# cdl - cd into the dir where a symlink's destination is
cdl() {
    cd "$(dirname "$(readlink $@)")"
}

# venv
venv() {
    subcmd="$1"
    shift

    if [[ -z "$@" ]]; then
        venvname=.venv
    else
        venvname="$@"
    fi

    if [[ $subcmd == "new" ]]; then
        python3 -m venv "$venvname"
    elif [[ $subcmd == "on" ]]; then
        . "$venvname/bin/activate"
    elif [[ $subcmd == "off" ]]; then
        deactivate
    else
        echo "$0: Unknown command"
    fi
}

## GIT SCRIPTS - executes "git <rm|mv|add> instead of rm|mv|add when you're in a git directory
_gitscripts() {
    if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" ]; then
        echo "-=- $1 running on git repo -=-"
        git $@
    else
        $@
    fi
}

alias grm="_gitscripts rm"
alias gmv="_gitscripts mv"
alias gadd="git add"

## VIM KEYS ##
bindkey -v
bindkey '^?' backward-delete-char

## PLUGINS ##
if [ ! -d "$antigen_dir" ]; then
    mkdir $antigen_dir
    curl -L git.io/antigen > $antigen_dir/antigen.zsh
fi

autoload -Uz zmv
#autoload -Uz vcs_info
#precmd_vcs_info() { vcs_info }
#precmd_functions+=( precmd_vcs_info )

. $antigen_dir/antigen.zsh

antigen bundle 'zsh-users/zsh-syntax-highlighting'
antigen bundle 'zsh-users/zsh-autosuggestions'
antigen apply

## ALIASES ##
eza_args="eza --icons --sort=type -lh --color=always"
alias ls="$eza_args"
alias ll="$eza_args -a"
alias tree="$eza_args -a --tree"
# program shorthands
alias hx='helix'
alias vi='nvim'

session_type="$(loginctl show-session $(awk '/tty/ {print $1}' <(loginctl)) -p Type | awk -F= '{print $2}')"
wayland_code_flags='--enable-features=UseOzonePlatform --ozone-platform=wayland'

[ $session_type = "wayland" ] && code_flags=$wayland_code_flags

alias c="code $code_flags"
alias nc="code -n $code_flags"

alias aptupd="sudo apt update"
alias aptupg="sudo apt upgrade"
alias aptup="aptupd && aptupg"

alias aptadd="sudo apt install"
alias aptrm="sudo apt purge"
alias aptar="sudo apt autoremove"
alias aptfix="sudo apt --fix-broken install"

# program alternatives
alias vim='nvim'
alias cat='bat'
alias neofetch="fastfetch"
# program arguments
alias less='less -r'
# editing configs
alias hxc='helix $XDG_CONFIG_HOME/helix/config.toml'
alias vrc='vim $XDG_CONFIG_HOME/nvim/init.lua'
# git bare repository
alias config="git --git-dir=$HOME/.cfg --work-tree=$HOME"
# cding into parent directories
alias ..='up'
alias ...='up 2'
alias ....='up 3'
alias .....='up 4'
alias ......='up 5'
alias .......='up 6'
alias ........='up 7'
alias .........='up 8'
alias ..........='up 9'
alias ...........='up 10'
# [duc disk usage viewer]
alias ducgui='duc gui /'
alias ducreload='duc index --one-file-system /'
alias ducinfo='duc info'
# duc commands at pwd
alias duchere='duc gui .'
alias ducreloadhere='duc index .'
# flatpaks
#alias slade="flatpak run net.mancubus.SLADE"
alias obs="flatpak run com.obsproject.Studio"
# mkdir
alias mkdir="mkdir -p"
# source zshrc
alias re.=". $HOME/.zshrc"
# doom emacs
alias emacs='doom run'
# emacs terminal
alias em='emacs -nw'

## PROMPT ##
#eval `starship init zsh` # turned off bc its too slow
#COLOR_BLACK="$(tput setaf 0)"
#COLOR_RED="$(tput setaf 1)"
#COLOR_GREEN="$(tput setaf 2)"
#COLOR_YELLOW="$(tput setaf 3)"
#COLOR_BLUE="$(tput setaf 4)"
#COLOR_MAGENTA="$(tput setaf 5)"
#COLOR_CYAN="$(tput setaf 6)"
#COLOR_WHITE="$(tput setaf 7)"
#
#BGCOLOR_BLACK="$(tput setab 0)"
#BGCOLOR_RED="$(tput setab 1)"
#BGCOLOR_GREEN="$(tput setab 2)"
#BGCOLOR_YELLOW="$(tput setab 3)"
#BGCOLOR_BLUE="$(tput setab 4)"
#BGCOLOR_MAGENTA="$(tput setab 5)"
#BGCOLOR_CYAN="$(tput setab 6)"
#BGCOLOR_WHITE="$(tput setab 7)"
#
#RESET="$(tput sgr0)"
#
#ESC=$(printf "\e")
#
#prompt_exitstatus() {
#   if [[ $? == 0 ]]; then
#       echo "${COLOR_GREEN}"
#   else
#       echo "with ${BGCOLOR_RED}$?${COLOR_RED}"
#   fi
#}

#zstyle ':vcs_info:git:*' formats '%b '

setopt PROMPT_SUBST
setopt INTERACTIVE_COMMENTS
PROMPT='%~ %(?.%F{green}.%F{red}%? )%B%#%f%b '
