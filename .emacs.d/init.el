;;
;;   ___ _ __ ___   __ _  ___ ___
;;  / _ \ '_ ` _ \ / _` |/ __/ __|
;; |  __/ | | | | | (_| | (__\__ \ config
;;  \___|_| |_| |_|\__,_|\___|___/
;; by thatonepuggo

;;; package stuff
(require 'package)
;; emacs package archives
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(unless (>= emacs-major-version 18)
  (add-to-list 'package-archives
               '("nongnu" . "https://elpa.nongnu.org/nongnu") t))
(package-initialize)
;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;;; macros
(fset 'mark-whole-line
      (kmacro-lambda-form [?\C-a ?\C-  ?\C-e] 0 "%d"))
(fset 'swap-chars
      (kmacro-lambda-form [?\S-\C-f ?\C-w ?\C-f ?\C-y] 0 "%d"))

;; be-evil: use vim keybindings and replace some packages?
(setq be-evil t)

(if be-evil
    (progn
      (use-package evil
        :ensure t
        :bind (:map evil-normal-state-map
                    ("U" . evil-redo)
                    ("-" . recompile))
        :config
        (evil-mode)
        (evil-set-undo-system 'undo-redo))

      (use-package evil-mc
        :ensure t
        :config
        (global-evil-mc-mode 1)))
  ;; else...
  (use-package paredit
    :ensure t
    :bind (:map paredit-mode-map
                ("M-w" . paredit-copy-as-kill))
    :hook ((emacs-lisp-mode . enable-paredit-mode)
           (eval-expression-minibuffer-setup . enable-paredit-mode)
           (ielm-mode . enable-paredit-mode)
           (lisp-mode . enable-paredit-mode)
           (lisp-interaction-mode . enable-paredit-mode)
           (scheme-mode . enable-paredit-mode))
    :config
    (autoload 'enable-paredit-mode "paredit"
      "Turn on pseudo-structural editing of Lisp code." t))
  ;; multiple-cursors
  (use-package multiple-cursors
    :ensure t
    :bind (("C->" . mc/mark-next-like-this)
           ("C-<" . mc/mark-previous-like-this)
           ("C-*" . mc/mark-all-like-this)))
;;; keys
  ;; mark whole line
  (global-set-key (kbd "C-x j") #'mark-whole-line))


(use-package rainbow-mode :ensure t)

;; vertico
(use-package vertico
  :ensure t
  :config
  (vertico-mode))

(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless)
        read-buffer-completion-ignore-case t))

(use-package marginalia
  :ensure t
  :config
  (marginalia-mode))
;; which key
(use-package which-key
  :ensure t
  :config
  (which-key-mode))

;; counsel
(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x))
  :config
  (counsel-mode))

;; lsp
(use-package lsp-mode
  :ensure t
  :bind (("C-c d" . lsp-describe-thing-at-point)
         ("C-c a" . lsp-execute-code-action)
         :map lsp-mode-map
         ("C-c l" . lsp-command-map))
  :config
  (lsp-enable-which-key-integration t))

;; company
(use-package company
  :ensure t
  :config
  (global-company-mode))

;; scratch buffer
(use-package immortal-scratch
  :ensure t
  :config
  (immortal-scratch-mode))

(setq inhibit-startup-screen t)

;; treemacs
(use-package treemacs
  :ensure t
  :bind (("C-t" . treemacs)))

;; drag-stuff
(use-package drag-stuff
  :ensure t
  :bind (("M-S-<up>" . drag-stuff-up)
         ("M-S-<down>" . drag-stuff-down)
         ("M-S-<left>" . drag-stuff-left)
         ("M-S-<right>" . drag-stuff-right)))

;; org bullets mode
(use-package org-bullets
  :ensure t
  :hook ((org-mode . org-bullets-mode)))

;; themes
;; some themes:
;;  - gruvbox-theme
;;  - gruber-darker-theme
;;  - zenburn-theme
(use-package gruvbox-theme
  :ensure t
  :config
  (load-theme 'gruvbox t))
;; smart tabs
;;(use-package smart-tabs-mode
;;  :ensure t
;;  :config
;;  (smart-tabs-insinuate 'c  'javascript))

(setq-default indent-tabs-mode nil
              tab-width 4)
(add-hook 'c-mode-common-hook
          (lambda () (setq indent-tabs-mode t)))

;; language/config modes
(use-package haskell-mode :ensure t)
(use-package rust-mode :ensure t)
(use-package go-mode :ensure t)
(use-package d-mode :ensure t)
(use-package glsl-mode :ensure t)
(use-package nim-mode :ensure t)
(use-package json-mode :ensure t)
(use-package vimrc-mode :ensure t)
(use-package yaml-mode :ensure t)
(use-package lua-mode :ensure t)
(use-package ini-mode :ensure t)
;; lsp
(use-package lsp-haskell :ensure t)

;;; global vars
(setq use-dialog-box nil)
;; change auto save dir - https://www.reddit.com/r/emacs/comments/azqpjj/how_do_i_make_emacs_stop_autosaving/
(setq temporary-file-directory "~/.emacs.d/temp/"
      backup-directory-alist `((".*" . ,(concat temporary-file-directory "/backups")))
      auto-save-file-name-transforms `((".*" ,(concat temporary-file-directory "/autosaves") t)))
;; 80 column rule
(setq fill-column 80)
(global-display-fill-column-indicator-mode t)
;; line numbers
(setq-default display-line-numbers 'visual
              display-line-numbers-type 'visual
              global-display-line-numbers-mode t)


;;; font(s)
;(set-face-attribute 'default nil :font "IosevkaNerdFont-15")
(set-face-attribute 'default nil :font "FiraCodeNerdFont-10")
;(set-fontset-font t 'symbol "Apple Color Emoji")
(set-fontset-font t 'symbol "Noto Font Emoji")
;;; modes
;; disable some GUI stuff
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
;; other stuff
(auto-save-visited-mode t)
(show-paren-mode t)
;; global modes
(global-hl-line-mode t)


;; emacs customize stuffs
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("9e3ea605c15dc6eb88c5ff33a82aed6a4d4e2b1126b251197ba55d6b86c610a1" "6b5c518d1c250a8ce17463b7e435e9e20faa84f3f7defba8b579d4f5925f60c1" "83e0376b5df8d6a3fbdfffb9fb0e8cf41a11799d9471293a810deb7586c131e6" "7661b762556018a44a29477b84757994d8386d6edee909409fabe0631952dad9" "d14f3df28603e9517eb8fb7518b662d653b25b26e83bd8e129acea042b774298" default))
 '(package-selected-packages
   '(evil-mc org-bullets org-bullets-mode ini-mode paredit company-mode marginalia orderless rainbow-mode which-key lsp-mode vertico evil immortal-scratch counsel treemacs-all-the-icons treemacs json-mode vimrc-mode gruvbox-theme yaml-mode company nim-mode markdown-mode tuareg haskell-mode go-mode magit multiple-cursors zenburn-theme d-mode glsl-mode rust-mode gruber-darker-theme drag-stuff)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'downcase-region 'disabled nil)
