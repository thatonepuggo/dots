#!/bin/sh
verbose=true
# Terminate already running bar instances
killall polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch polybar
if type "xrandr"; then
    for m in $(xrandr --query | grep " connected" | awk '{print $1}'); do
        [ verbose = true ] && echo "monitor: $m"
        MONITOR=$m polybar --reload main &
    done
else
    polybar --reload main &
fi

