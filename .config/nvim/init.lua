require 'plugins'
local util = require 'util'
require 'mapper'
require 'lspconfigconfig'
require 'suda'
vim.cmd [[colorscheme bamboo]]
vim.o.wrap = false
-- mouse
vim.o.mouse = 'a'
-- set both number and relativenumber
vim.o.number = true
vim.o.relativenumber = true
-- set encoding to utf8
vim.o.encoding = "UTF-8"
-- [TAB] -> 4 spaces (unless editing in makefile of course)
vim.o.tabstop = 4
vim.o.softtabstop = 0
vim.o.shiftwidth = 4
vim.o.smarttab = true
vim.o.expandtab = true
-- 80 column rule
vim.o.colorcolumn = "80"
-- disable folding (annoying in VDF files)
vim.o.foldenable = false

-- the cursor column
vim.o.cursorcolumn = true
-- set the clipboard to use the system clipboard
vim.o.clipboard = "unnamedplus"
-- dont check to run comments as vimscript
vim.o.modeline = false
-- tab completion
vim.o.wildmode = "longest,list,full"
vim.o.wildmenu = true

vim.o.signcolumn = "yes:1"

-- gui

vim.g.neovide_cursor_vfx_mode = "pixiedust"
vim.g.neovide_transparency = 0.975
vim.o.guifont = "Iosevka:h15"

vim.g.mapleader = " "

require 'lualine'.setup({
    options = {
--        section_separators = { left = '', right = '' },
--        component_separators = { left = '', right = '' }
        section_separators = { left = ' ', right = ' ' },
        component_separators = { left = ' ', right = ' ' }
    }
})

-- transparency
--[[
require "transparent".setup({ -- Optional, you don't have to run setup.
  groups = { -- table: default groups
    'Normal',
    'NormalNC',
    'Comment',
    'Constant',
    'Special',
    'Identifier',
    'Statement',
    'PreProc',
    'Type',
    'Underlined',
    'Todo',
    'String',
    'Function',
    'Conditional',
    'Repeat',
    'Operator',
    'Structure',
    'LineNr',
    'NonText',
    'SignColumn',
    'CursorLine',
    'CursorLineNr',
    'EndOfBuffer',
  },
  extra_groups = {}, -- table: additional groups that should be cleared
  exclude_groups = {
        'StatusLine',
        'StatusLineNC',
  }, -- table: groups you don't want to clear
})
vim.cmd [[TransparentEnable]] -- why???



--
-- autocmds
--

local augroup = vim.api.nvim_create_augroup
local aucmd = vim.api.nvim_create_autocmd

local group_stripper = augroup("stripper", { clear = true })
local group_comment = augroup("comment", { clear = true})

-- center cursor
aucmd("InsertEnter", {
    pattern = '*',
    command = 'norm zz',
})

-- on write, remove trailing whitespace
aucmd("BufWritePre", {
    pattern = '*',
    command = '%s/\\s\\+$//e',
})

-- stripper:source
aucmd("BufReadPost", {
    pattern = { "*/addons/stripper/global_filters.cfg", "*/addons/stripper/maps/*.cfg" },
    group = group_stripper,
    callback = function()
        vim.bo.filetype = "vdf"
        vim.bo.expandtab = false
        vim.bo.smarttab = false
    end,
})

aucmd("BufReadPost", {
    pattern = "*",
    group = group_comment,
    callback = function()
        -- disable auto comments
        vim.opt.formatoptions:remove { "c", "r", "o" }
    end,
})

--
-- end autocmds
--

--
-- bindings
--
-- add :noh keybinding
nmap '<leader>nh' ':noh<CR>'

-- goyo keybinding
--nmap '<leader><space>' ':Goyo<CR>'

-- remap control-r to U (redo)
nnoremap 'U' '<C-R>'

-- fzf
nnoremap '<C-p>' ':GFiles<CR>'

-- Toggle auto comments
map '<leader>c' ':setlocal formatoptions-=cro<CR>'
map '<leader>C' ':setlocal formatoptions=cro<CR>'

-- coc
nmap '<leader>gd' "<Plug>(coc-definition)"
nmap '<leader>gr' "<Plug>(coc-references)"

-- delete no clipboard
vmap '<leader>d' '"_d'
nmap '<leader>dl' '"_dl'
nmap '<leader>dd' '"_dd'
map '<leader>D' '"_D'

-- select all
map '<leader>a' 'ggVG'

-- lspconfig
nnoremap '<leader>ga' '<cmd>lua vim.lsp.buf.code_action()<CR>'

--
-- commands
--
vim.api.nvim_create_user_command(
    'DiffOrig',
    "vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis | wincmd p | diffthis",
    {}
)
