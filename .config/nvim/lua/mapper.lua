local _noremap = { noremap = true }

local function _map_new(mode, opts)
    local opts = opts or {}
    return function(lhs)
        return function(rhs)
            vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
        end
    end
end


map =  _map_new('')
nmap = _map_new('n')
vmap = _map_new('v')
imap = _map_new('i')

noremap =  _map_new('', _noremap)
nnoremap = _map_new('n', _noremap)
vnoremap = _map_new('v', _noremap)
inoremap = _map_new('i', _noremap)
