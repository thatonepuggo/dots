local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    -- syntax highlighting
    'rust-lang/rust.vim',
    'fladson/vim-kitty',
    'kovetskiy/sxhkd-vim',
    'waycrate/swhkd-vim',
    'ziglang/zig.vim',
    'rpdelaney/vim-sourcecfg',
    'Fymyte/rasi.vim',
    'nvim-treesitter/nvim-treesitter',
    'withgod/vim-sourcepawn',
    -- lualine
    'nvim-lualine/lualine.nvim',
    'ryanoasis/vim-devicons',
    -- theme
    'morhetz/gruvbox',
    { 'catppuccin/nvim', as = 'catppuccin' },
    'luochen1990/rainbow',
    'ribru17/bamboo.nvim',
    'fneu/breezy',
    -- transparency
    'xiyaowong/transparent.nvim',
    -- coc
    { 'neoclide/coc.nvim', branch = 'release' },
    -- goyo
    --'junegunn/goyo.vim',
    -- emmet
    'mattn/emmet-vim',
    -- multiple cursors
    'terryma/vim-multiple-cursors',
    -- lsp
    'neovim/nvim-lspconfig',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'hrsh7th/cmp-cmdline',
    'hrsh7th/nvim-cmp',
    -- vsnip
    --'hrsh7th/cmp-vsnip',
    'hrsh7th/vim-vsnip',
    -- sudo
    'lambdalisue/vim-suda',
})
