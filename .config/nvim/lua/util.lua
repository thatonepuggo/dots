local util = {}

function util.all(...)
	local tab = {...}
	local ret = true
	for _, v in ipairs(tab) do
		if not v then ret = false end
	end
	return ret
end

function util.any(...)
	local tab = {...}
	local ret = false
	for _, v in ipairs(tab) do
		if v then ret = true end
	end
	return ret
end

return util
